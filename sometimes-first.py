# TO-DO:
# 1. Add --headless option [DONE]
# 2. Update to only loop refresh until token appears - then break loop and submit [DONE]

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

# create a new instance of the Chrome driver
driver = webdriver.Chrome()

# navigate to the login page
driver.get('https://sis.galvanize.com/cohorts/20/attendance/mine/')

# click single-login
button = driver.find_element(By.XPATH, '//button[@class="button is-info"]')
button.click()

# find the email input field and enter your email address
email_field = driver.find_element(By.ID, 'user_email')
email_field.send_keys('test@test.com')

# wait for the password input field to appear and enter your password
password_field = driver.find_element(By.ID, 'user_password')
password_field.send_keys('test')
password_field.send_keys(Keys.RETURN)

# refresh after login
driver.get('https://sis.galvanize.com/cohorts/20/attendance/mine/')

# refresh page until token appears and submit loop
num_refreshes = 0
while True:
    driver.refresh()
    try:
        # find token
        find_token = driver.find_element(By.XPATH, '//*[@class="tag is-danger is-size-6"]') # noqa
        # extract token
        token_code = find_token.text
        break
    except: # noqa
        # if the target value is not found, increment the counter and continue the loop # noqa
        num_refreshes += 1
        print(num_refreshes)

# find input field
input_field = driver.find_element(By.ID, 'form-token')

# enter token
input_field.send_keys(token_code)

# find submit button
submit_button = driver.find_element(By.XPATH, '//*[@class="button is-primary"]') # noqa

# click button
submit_button.click()
print(f"Token submitted, the code is: {token_code}")

# close the browser window
# driver.quit()
